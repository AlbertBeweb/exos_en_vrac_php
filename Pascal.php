//****Triangle de Pascal*******//
<?php
//Declaration des variables
$hauteur = 5;
$decalage = $hauteur - 1;

   // Définition des lignes en fonction "hauteur"
   for($i = 0; $i < $hauteur; $i++){
       $ligne = "x";
       //Nombre de caractères par ligne
       $b = $i * 2 . 1;

       //Ajout des étoiles
       for($j = 0; $j < $b; $j++){
           $ligne = $ligne . "*";

       //Ajout de l'espace
       }
       for($k = 0; $k < $decalage; $k++){
           $ligne = "x" . $ligne;
       }
       //Décrécrémentation
       $decalage--;

       //Affichage de la pyramide de Pascal
       echo $ligne;
   }
?>